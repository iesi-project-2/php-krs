<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <title>Tambah Mata Kuliah</title>
</head>

<body>
    <form action="c_search_mk_krs.php" method="post">
        <label>Kode Matakuliah</label>
        <input type="text" name="kode_mk">
        <br><br>
        <button type="submit" name="kirim_cari_kode_mk">Submit</button>

    </form>
    <table>
        <tr>
            <th>Kode</th>
            <th>Nama Mata Kuliah</th>
            <th>SKS</th>
            <th>Kelas</th>
            <th>Isi Kelas</th>
            <th>Hari</th>
            <th>Jam</th>
            <th>Action</th>
        </tr>
        <?php
        if (isset($jadwal)) {
            foreach ($jadwal as $nilai) {
                echo "<tr>";
                echo "<td>$nilai[kode_mk]</td>";
                echo "<td>$nilai[nama_mk]</td>";
                echo "<td>$nilai[sks]</td>";
                echo "<td>$nilai[kelas]</td>";
                echo "<td>$nilai[isi_kelas]</td>";
                echo "<td>$nilai[hari]</td>";
                echo "<td>$nilai[jam]</td>";
                echo "<td><form action='c_tambah.php' method='post'>
                <input type='hidden' name='id_mk' value=$nilai[id_mk]>
                <input class='hapus' type='submit' name='kirim_tambah_mk' value='Tambah jadwal KRS'>
            </form></td>";
                echo "</tr>";
            }
        }
        ?>
    </table>
</body>

</html>