<?php
include_once 'm_krs.php';
class c_krs
{
    public $model;
    public function __construct()
    {
        $this->model = new m_krs();
    }
    public function invoke()
    {
        $matkul = $this->model->getAll();
        // var_dump($this->model);
        include 'v_krs.php';
    }
    public function add($id_mk)
    {
        $this->model->setKrs($id_mk);
    }
    public function delete($id_krs)
    {
        $this->model->deleteKrs($id_krs);
    }

    public function getSelectedData()
    {
        $data = $this->model->getAll();
        include 'v_tambah.php';
    }
    public function cariMatkulTambahKRs($kode_mk)
    {
        $jadwal = $this->model->cariMatkul($kode_mk);
        include 'v_tambah.php';
    }
    public function cariMatkul($kode_mk)
    {
        $jadwal = $this->model->cariMatkul($kode_mk);
        include 'v_jadwal.php';
    }
}
