USE iesi;

CREATE TABLE `jadwal` (
  `id_jadwal` int(11) NOT NULL,
  `jam` varchar(255) DEFAULT NULL,
  `kelas` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `jadwal` (`id_jadwal`, `jam`, `kelas`) VALUES
(1, '07.00 - 08.30', 'A'),
(2, '07.00 - 08.30', 'B'),
(3, '08.30 - 10.00', 'A'),
(4, '08.30 - 10.00', 'B'),
(5, '10.00 - 11.00', 'A'),
(6, '10.00 - 11.00', 'B'),
(7, '13.00 - 14.00', 'A'),
(8, '13.00 - 14.00', 'B'),
(9, '13.00 - 15.00', 'A'),
(10, '13.00 - 15.00', 'B'),
(11, '14.00 - 15.30', 'A'),
(12, '14.00 - 15.30', 'B'),
(13, '15.30 - 17.00', 'A'),
(14, '15.30 - 17.00', 'B'),
(15, '15.00 - 17.00', 'A'),
(16, '15.00 - 17.00', 'B');


CREATE TABLE `mata_kuliah` (
  `id_mk` int(100) NOT NULL,
  `kode_mk` varchar(255) NOT NULL,
  `nama_mk` varchar(255) NOT NULL,
  `sks` int(100) NOT NULL,
  `kelas` varchar(255) NOT NULL,
  `isi_kelas` int(100) NOT NULL,
  `kuota` int(100) NOT NULL,
  `hari` varchar(255) NOT NULL,
  `jam` varchar(255) NOT NULL,
  `id_jadwal` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


INSERT INTO `mata_kuliah` (`id_mk`, `kode_mk`, `nama_mk`, `sks`, `kelas`, `isi_kelas`, `kuota`, `hari`, `jam`, `id_jadwal`) VALUES
(1, 'COM60014', 'Pemrogaman Dasar', 4, 'A', 36, 40, 'Selasa', '15.00 - 17.00', 15),
(2, 'COM60014', 'Pemrogaman Dasar', 4, 'B', 13, 40, 'Selasa', '13.00 - 15.00', 10),
(3, 'UBU60004', 'Bahasa Inggris', 2, 'A', 7, 40, 'Selasa', '10.00 - 11.00', 5),
(4, 'UBU60004', 'Bahasa Inggris', 2, 'B', 40, 40, 'Rabu', '10.00 - 11.00', 6),
(5, 'MPK60006', 'Kewarganegaraan', 2, 'A', 3, 40, 'Jumat', '13.00 - 14.00', 7),
(6, 'MPK60006', 'Kewarganegaraan', 2, 'B', 24, 40, 'Kamis', '13.00 - 14.00', 8),
(7, 'CIS61001', 'Arsitektur Komputer dan Sistem Operasi', 3, 'A', 40, 40, 'Rabu', '07.00 - 08.30', 1),
(8, 'CIS61001', 'Arsitektur Komputer dan Sistem Operasi', 3, 'B', 18, 40, 'Rabu', '08.30 - 10.00', 4),
(9, 'COM60015', 'Matematika Komputasi', 3, 'A', 25, 40, 'Rabu', '07.00 - 08.30', 1),
(10, 'COM60015', 'Matematika Komputasi', 3, 'B', 20, 40, 'Rabu', '08.30 - 10.00', 4),
(11, 'CIS61002', 'Manajemen Bisnis Fungsional', 3, 'A', 15, 40, 'Kamis', '15.30 - 17.00', 16),
(12, 'CIS61002', 'Manajemen Bisnis Fungsional', 3, 'B', 17, 40, 'Kamis', '14.00 - 15.30', 12),
(13, 'COM60016', 'Pengantar Keilmuan Komputer', 2, 'A', 10, 40, 'Kamis', '13.00 - 14.00', 7),
(14, 'COM60016', 'Pengantar Keilmuan Komputer', 2, 'B', 40, 40, 'Jumat', '13.00 - 14.00', 8),
(15, 'MPK60007', 'Bahasa Indonesia', 2, 'A', 34, 40, 'Rabu', '10.00 - 11.00', 5),
(16, 'MPK60007', 'Bahasa Indonesia', 2, 'B', 7, 40, 'Selasa', '13.00 - 14.00', 5),
(17, 'CIS61008', 'Algoritma dan Struktur Data', 3, 'A', 39, 40, 'Jumat', '14.00 - 15.30', 11),
(18, 'CIS61008', 'Algoritma dan Struktur Data', 3, 'B', 16, 40, 'Senin', '14.00 - 15.30', 12),
(19, 'CIS61009', 'Dasar Desain Antarmuka Pengguna', 3, 'A', 32, 40, 'Senin', '14.00 - 15.30', 11),
(20, 'CIS61009', 'Dasar Desain Antarmuka Pengguna', 3, 'B', 13, 40, 'Jumat', '14.00 - 15.30', 12),
(21, 'CIS61010', 'Administrasi Basis Data', 4, 'A', 3, 40, 'Selasa', '13.00 - 15.00', 9),
(22, 'CIS61010', 'Administrasi Basis Data', 4, 'B', 17, 40, 'Selasa', '15.00 - 17.00', 16),
(23, 'CIS61011', 'Manajemen Investasi Teknologi Informasi', 3, 'A', 9, 40, 'Jumat', '07.00 - 08.30', 1),
(24, 'CIS61011', 'Manajemen Investasi Teknologi Informasi', 3, 'B', 17, 40, 'Senin', '07.00 - 08.30', 2),
(25, 'CIS61012', 'Tata Kelola Teknologi Informasi', 3, 'A', 12, 40, 'Senin', '07.00 - 08.30', 1),
(26, 'CIS61012', 'Tata Kelola Teknologi Informasi', 3, 'B', 36, 40, 'Jumat', '07.00 - 08.30', 2),
(27, 'CIS61013', 'Dasar Pengembangan Sistem Informasi', 2, 'A', 12, 40, 'Senin', '10.00 - 11.00', 5),
(28, 'CIS61013', 'Dasar Pengembangan Sistem Informasi', 2, 'B', 37, 40, 'Jumat', '10.00 - 11.00', 6),
(29, 'COM60052', 'Etika Profesi', 2, 'A', 21, 40, 'Jumat', '10.00 - 11.00', 5),
(30, 'COM60052', 'Etika Profesi', 2, 'B', 14, 40, 'Senin', '10.00 - 11.00', 6),
(31, 'CIS61020', 'Pengembangan Aplikasi Perangkat Bergerak', 3, 'A', 33, 40, 'Jumat', '08.30 - 10.00', 3),
(32, 'CIS61020', 'Pengembangan Aplikasi Perangkat Bergerak', 3, 'B', 18, 40, 'Senin', '08.30 - 10.00', 4),
(33, 'CIS61021', 'Pengantar Sains Data', 2, 'A', 37, 40, 'Rabu', '13.00 - 14.00', 7),
(34, 'CIS61021', 'Pengantar Sains Data', 2, 'B', 2, 40, 'Senin', '13.00 - 14.00', 8),
(35, 'CIS61022', 'Teknik Analisis Kuantitatif dan Kualitatif Sistem Informasi', 3, 'A', 24, 40, 'Senin', '08.30 - 10.00', 3),
(36, 'CIS61022', 'Teknik Analisis Kuantitatif dan Kualitatif Sistem Informasi', 3, 'B', 8, 40, 'Jumat', '08.30 - 10.00', 4),
(37, 'CIS61023', 'Implementasi dan Evaluasi Sistem Informasi', 3, 'A', 25, 40, 'Rabu', '14.00 - 15.30', 11),
(38, 'CIS61023', 'Implementasi dan Evaluasi Sistem Informasi', 3, 'B', 31, 40, 'Rabu', '15.30 - 17.00', 14),
(39, 'CSD60005', 'Manajemen Proyek Sistem Informasi', 3, 'A', 33, 40, 'Kamis', '14.00 - 15.30', 11),
(40, 'CSD60005', 'Manajemen Proyek Sistem Informasi', 3, 'B', 38, 40, 'Kamis', '15.30 - 17.00', 14),
(41, 'COM60051', 'Metode Penelitian dan Penulisan Ilmiah', 3, 'A', 39, 40, 'Rabu', '15.30 - 17.00', 13),
(42, 'COM60051', 'Metode Penelitian dan Penulisan Ilmiah', 3, 'B', 39, 40, 'Rabu', '14.00 - 15.30', 11),
(43, 'CIS61025', 'Big Data dan Analitik', 2, 'B', 15, 40, 'Kamis', '10.00 - 11.00', 13),
(44, 'CSD60015', 'Sistem Pendukung Keputusan', 3, 'A', 20, 40, 'Jumat', '15.30 - 17.00', 14),
(45, 'CSD60015', 'Sistem Pendukung Keputusan', 3, 'B', 4, 40, 'Senin', '15.30 - 17.00', 13),
(46, 'CIS61027', 'Pengembangan Berorientasi Penggunaan Ulang', 3, 'A', 40, 40, 'Senin', '15.30 - 17.00', 14),
(47, 'CIS61027', 'Pengembangan Berorientasi Penggunaan Ulang', 3, 'B', 30, 40, 'Jumat', '15.30 - 17.00', 3),
(48, 'CIS61028', 'Sistem Enterprise', 3, 'A', 37, 40, 'Selasa', '08.30 - 10.00', 2),
(49, 'CIS61028', 'Sistem Enterprise', 3, 'B', 26, 40, 'Selasa', '07.00 - 08.30', 1),
(50, 'CSD60014', 'Pengantar Geo Informasi', 3, 'A', 12, 40, 'Selasa', '07.00 - 08.30', 4),
(51, 'CSD60014', 'Pengantar Geo Informasi', 3, 'B', 39, 40, 'Selasa', '08.30 - 10.00', 1),
(52, 'CSD60010', 'Geo Informasi Lanjut 2', 3, 'A', 31, 40, 'Kamis', '07.00 - 08.30', 4),
(53, 'CSD60010', 'Geo Informasi Lanjut 2', 3, 'B', 30, 40, 'Kamis', '08.30 - 10.00', 5),
(54, 'CSD60008', 'E-Government', 2, 'A', 39, 40, 'Kamis', '10.00 - 11.00', 1),
(55, 'CIS61032', 'Manajemen Risiko dan Kontrol Sistem Informasi', 3, 'A', 37, 40, 'Kamis', '07.00 - 08.30', 4),
(56, 'CIS61032', 'Manajemen Risiko dan Kontrol Sistem Informasi', 3, 'B', 20, 40, 'Kamis', '08.30 - 10.00', 7),
(57, 'CIS61033', 'Technopreneurship', 2, 'A', 34, 40, 'Senin', '13.00 - 14.00', 8),
(58, 'CIS61033', 'Technopreneurship', 2, 'B', 26, 40, 'Rabu', '13.00 - 14.00', 6);



DROP TABLE KRS;
CREATE TABLE `krs` (
  `id_krs` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `id_row_mk` INT NOT NULL,
  `kode_mk` VARCHAR(255) NOT NULL,
  UNIQUE (id_row_mk, kode_mk),
  FOREIGN KEY (id_row_mk) REFERENCES mata_kuliah(id_mk)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`id_jadwal`);


ALTER TABLE `mata_kuliah`
  ADD PRIMARY KEY (`id_mk`),
  ADD KEY `id_jadwal` (`id_jadwal`);


ALTER TABLE `mata_kuliah`
  ADD CONSTRAINT `mata_kuliah_ibfk_1` FOREIGN KEY (`id_jadwal`) REFERENCES `jadwal` (`id_jadwal`);
COMMIT;

