<?php
require "koneksiMVC.php";
class m_krs
{
    private $database;
    public function __construct()
    {
        $this->database = new koneksiMVC();
    }

    function execute($query)
    {
        $connect = $this->database->mysqli;
        return mysqli_query($connect, $query);
    }

    function fetch($var)
    {
        return mysqli_fetch_array($var);
    }

    public function getAll()
    {
        $query = "SELECT krs.id_krs as id_krs ,mata_kuliah.kode_mk as kode_mk , mata_kuliah.nama_mk as nama_mk, mata_kuliah.kelas as kelas FROM krs INNER JOIN mata_kuliah ON krs.id_row_mk = mata_kuliah.id_mk";
        return $this->execute($query);
    }

    public function setKrs($id_mk)
    {
        $query = "INSERT INTO krs (id_krs,id_row_mk) VALUES (default,$id_mk)";
        return $this->execute($query);
    }

    public function deleteKrs($id_krs)
    {
        $query = "DELETE FROM krs WHERE id_krs = $id_krs";
        return $this->execute($query);
    }

    public function cariMatkul($cari_kode_mk)
    {
        $query = "SELECT id_mk, kode_mk, nama_mk, sks, kelas, isi_kelas, hari, jam
                      FROM mata_kuliah WHERE kode_mk LIKE '%$cari_kode_mk%'";
        return $this->execute($query);
    }
}
